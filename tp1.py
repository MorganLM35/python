prixTotalTTC=0
cpt=0

cptProduit = int(input('Combien de produits voulez vous saisir ? :'))

while(cpt != cptProduit):


    prixHt = float(input('Saisir un prix HT : '))
    while(prixHt<=0):
        prixHt = float(input('Le prix HT doit être supérieur à 0, veuillez resaisir le prix : '))

    qte = int(input('Saisir une quantité : '))
    while(qte<=0):
        qte = int(input('La quantité doit être supérieur à 0, et doit être un entier, veuillez resaisir la qté : '))

    prixPdtTTC = prixHt*qte*1.2
    if(prixPdtTTC>200):
        prixPdtTTC=prixPdtTTC*0.95

    prixTotalTTC = prixTotalTTC+prixPdtTTC
    cpt = cpt + 1 


print ('Le prix total est de : ', prixTotalTTC ,'€')